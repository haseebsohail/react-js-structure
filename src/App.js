import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import Login from './container/Auth/sign-in';
import Signup from './container/Auth/sign-up';
import Loader from './components/loader';
import NotFound from './components/not-found';
class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      auth: !!this.props.logindata,
      page: false,
      logindata: this.props.logindata,
      clientCookie: false,
      logout: false,
      connection: '',
    }
    console.log("this.props.logindata", this.props.logindata);


  }
  componentDidMount() {

  }
  render() {
    let routes = "";

    // var routes = (
    //   !!this.props.logindata &&
    //   <Switch>
    //     <Redirect strict exact from="/" to="/home" />

    //     <Route component={NotFound} />
    //   </Switch>
    // );

    if (!this.props.logindata || this.props.logindata.length === 0) {
      routes = (
        <Switch>
          <Route exact path="/signup" component={Signup} />
          <Route exact path="/signin" component={Login} />
          <Redirect strict from="/" to="/signin" />
          <Route component={NotFound} />
        </Switch>
      );
    }

    return (
      <div className='main'>
        <Loader loading={this.props.loading} />
        {routes}
        <button onClick={() => console.log("this.props", this.props)}>
          click me please
        </button>
      </div >

    )
  }
}


const mapStateToProps = state => {
  return {
    logindata: state.auth.logindata,
    loading: state.loader.show
  }
}

const mapDispatchToProps = (dispatch) => {
  return {

  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));