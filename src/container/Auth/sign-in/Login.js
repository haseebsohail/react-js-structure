import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Input from '../../../components/input'
import './style.css'


class Login extends Component {
    render() {
        return (
            <div>
                <h2>Login Form</h2>

                <div className="imgcontainer">
                    {/* <img src="img_avatar2.png" alt="Avatar" className="avatar" /> */}
                </div>

                <div className="container">
                    <label ><b>Username</b></label>
                    <Input type="text" placeholder="Enter User Name" name="psw" required />

                    <label ><b>Password</b></label>
                    <Input type="password" placeholder="Enter Password" name="psw" required />

                    <button type="submit">Login</button>
                    <label>
                        <Input type="checkbox" checked="checked" name="remember" /> Remember me
                    </label>
                </div>

                <div className="container" style={{ backgroundColor: '#f1f1f1' }}>
                    <button type="button" className="cancelbtn">Cancel</button>
                    <span className="psw">Forgot password?</span>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        loading: state.loader.loading,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));