import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Input from '../../../components/input'

import './style.css'


class SignUp extends Component {
    render() {
        return (
            <div>
                <h2>Sign Form</h2>

                <div className="imgcontainer">
                    {/* <img src="img_avatar2.png" alt="Avatar" className="avatar" /> */}
                </div>

                <div className="container">
                    <label ><b>Username</b></label>
                    <Input type="text" placeholder="Enter Username" name="uname" required />
                    <label ><b>Email</b></label>
                    <Input type="email" placeholder="Enter Email" name="email" required />

                    <label ><b>Password</b></label>
                    <Input type="password" placeholder="Enter Password" name="psw" required />

                    <button type="submit">Sign Up</button>
                    <label>
                        <Input type="checkbox" checked="checked" name="remember" /> Remember me
                    </label>
                </div>

                <div className="container" style={{ backgroundColor: '#f1f1f1' }}>
                    <button type="button" className="cancelbtn">Cancel</button>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        loading: state.loader.loading,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SignUp));