import { baseUrl, authHeader, header } from "./sharedApi";
import { requestHelper } from "../utils/helper";

export const loginUser = async credentials => {
	const res = await requestHelper(
		`${baseUrl}/auth`,
		"POST",
		authHeader,
		credentials
	);
	const data = res.json();
	return data;
};

export const fetchDevice = async () => {
	let url = `${baseUrl}/devices`;
	const res = await requestHelper(url, "GET", authHeader);
	const data = await res.json();
	return data;
};

export const getInventoryList = async () => {
	let url = `${baseUrl}/inventory/list`;
	const res = await requestHelper(url, 'GET', await header());
	const data = await res.json();
	return data;
}
