import { getToken } from '../utils/helper'


export const baseUrl = "http://localhost:3000/api";
// export const baseUrl = "https://myDomain";
//export const baseUrl = "http://localhost:9000/api";

export const header = async () => ({
	"Content-Type": "application/json",
	"x-access-token": await getToken(),
	"Access-Control-Allow-Origin": "*"
});

export const authHeader = {
	"Content-Type": "application/json",
	"Access-Control-Allow-Origin": "*"
};
